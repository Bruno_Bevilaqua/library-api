/**
 * Interfaces for country
 */

export interface CountryData {
  id: number;
  name: string;
  fullName: string;
  short: string;
  flag: string;
}
